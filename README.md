# 2021 MathorCup B题 Au20 预测-寻优问题

## 介绍
包含 2021 MathorCup 的 Au20 能量预测问题，和最优结构求解问题的代码。

其中预测问题的文档：https://blog.csdn.net/weixin_42141390/article/details/115729199
优化问题的文档：https://blog.csdn.net/weixin_42141390/article/details/115834639

##  注意
本文没有写 BA45 和 AU25 的部分。这里只写了 AU20 的能量模型的建立，以及最优结构的搜索。

另外，文档写得非常的思路不清晰，还请大家见谅呀。
## 代码
由于这是本人第一次比较有序地写作，所以一些代码组织上没有之后几次清晰。所以，大家要尽量看注释的内容。

代码运行参考顺序：
1. read_data.py：可以不运行，就是画图用的
2. data_preprocess.py
3. machine_leanring_method.py：机器学习模型
4. deep_learning_method.py：多层感知器
5. conv_network.py：卷积神经网络
6. optimization_algorithm.py：遗传算法筛选最优结构